var doc = new jsPDF('l', 'pt');
var columnsLong = getColumns().concat([
    {title: shuffleSentence(), dataKey: "text"},
    {title: "Text with a\nlinebreak", dataKey: "text2"}
]);

doc.text("Overflow 'ellipsize' (default)", 10, 40);
doc.autoTable(columnsLong, getData(), {
    startY: 55,
    margin: {horizontal: 10},
    columnStyles: {text: {columnWidth: 250}}
});

doc.text("Overflow 'hidden'", 10, doc.autoTableEndPosY() + 30);
doc.autoTable(columnsLong, getData(), {
    startY: doc.autoTableEndPosY() + 45,
    margin: {horizontal: 10},
    styles: {overflow: 'hidden'},
    columnStyles: {email: {columnWidth: 160}}
});

doc.text("Overflow 'linebreak'", 10, doc.autoTableEndPosY() + 30);
doc.autoTable(columnsLong, getData(3), {
    startY: doc.autoTableEndPosY() + 45,
    margin: {horizontal: 10},
    styles: {overflow: 'linebreak'},
    bodyStyles: {valign: 'top'},
    columnStyles: {email: {columnWidth: 'wrap'}},
});