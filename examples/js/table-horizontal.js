var doc = new jsPDF('p', 'pt');
doc.autoTable(getColumns().splice(1,4), getData(), {
    drawHeaderRow: function() {
        // Don't draw header row
        return false;
    },
    columnStyles: {
        first_name: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold'}
    }
});