var doc = new jsPDF('p', 'pt');
doc.autoTable(getColumns(), getData(), {
    tableWidth: 'wrap',
    styles: {cellPadding: 2},
    headerStyles: {rowHeight: 15, fontSize: 8},
    bodyStyles: {rowHeight: 12, fontSize: 8, valign: 'middle'}
});