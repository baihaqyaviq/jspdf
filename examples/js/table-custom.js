var doc = new jsPDF('p', 'pt');
doc.autoTable(getColumns().slice(2, 6), getData(20), {
    styles: {
        font: 'courier',
        fillStyle: 'DF',
        lineColor: [44, 62, 80],
        lineWidth: 2
    },
    headerStyles: {
        fillColor: [44, 62, 80],
        fontSize: 15,
        rowHeight: 30
    },
    bodyStyles: {
        fillColor: [52, 73, 94],
        textColor: 240
    },
    alternateRowStyles: {
        fillColor: [74, 96, 117]
    },
    columnStyles: {
        email: {
            fontStyle: 'bold'
        }
    },
    createdCell: function (cell, data) {
        if (data.column.dataKey === 'expenses') {
            cell.styles.halign = 'right';
            if (cell.raw > 600) {
                cell.styles.textColor = [255, 100, 100];
                cell.styles.fontStyle = 'bolditalic';
            }
            cell.text = '$' + cell.text;
        } else if (data.column.dataKey === 'country') {
            cell.text = cell.raw.split(' ')[0];
        }
    }
});