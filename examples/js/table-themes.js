var doc = new jsPDF('p', 'pt');
doc.setFontSize(12);
doc.setFontStyle('bold');

doc.text('Theme "striped"', 40, 50);
doc.autoTable(getColumns(), getData(), {startY: 60});

doc.text('Theme "grid"', 40, doc.autoTableEndPosY() + 30);
doc.autoTable(getColumns(), getData(), {startY: doc.autoTableEndPosY() + 40, theme: 'grid'});

doc.text('Theme "plain"', 40, doc.autoTableEndPosY() + 30);
doc.autoTable(getColumns(), getData(), {startY: doc.autoTableEndPosY() + 40, theme: 'plain'});
