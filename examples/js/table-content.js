var doc = new jsPDF('p', 'pt');

doc.setFontSize(18);
doc.text('A story about Miusov', 40, 60);
doc.setFontSize(11);
doc.setTextColor(100);
var text = doc.splitTextToSize(shuffleSentence(faker.lorem.words(55)) + '.', doc.internal.pageSize.width - 80, {});
doc.text(text, 40, 80);

var cols = getColumns();
cols.splice(0, 2);
doc.autoTable(cols, getData(40), {startY: 150});

doc.text(text, 40, doc.autoTableEndPosY() + 30);