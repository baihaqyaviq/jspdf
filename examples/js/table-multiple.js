var doc = new jsPDF('p', 'pt');
doc.setFontSize(22);
doc.text("Multiple tables", 40, 60);
doc.setFontSize(12);

doc.autoTable(getColumns().slice(0, 3), getData(), {
    startY: 90,
    pageBreak: 'avoid',
    margin: {right: 305}
});

doc.autoTable(getColumns().slice(0, 3), getData(), {
    startY: 90,
    pageBreak: 'avoid',
    margin: {left: 305}
});

for (var j = 0; j < 6; j++) {
    doc.autoTable(getColumns(), getData(9), {
        startY: doc.autoTableEndPosY() + 30,
        pageBreak: 'avoid',
    });
}