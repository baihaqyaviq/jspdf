var doc = new jsPDF('p', 'pt');

var columns = [
    {title: "Kode", dataKey: "kode_mk"},
    {title: "Matakuliah", dataKey: "nm_mk"},
    {title: "SKS", dataKey: "sks_mk"},
    {title: "KLS", dataKey: "nm_kls"}
];

doc.setFontSize(12);
doc.setFontStyle('bold');
doc.text("KARTU RENCANA STUDI (KRS) MAHASISWA", 170, 100);

doc.setFontSize(11);
doc.setFontStyle('normal');
doc.text("Nim", 75, 125);
doc.text(": 8008080", 125, 125);
doc.text("Semester", 75, 140);
doc.text(": 5", 125, 140);

doc.text("Nama", 300, 125);
doc.text(": John Mahasiswa", 350, 125); doc.text("Prodi", 300, 140);
doc.text(": Manajemen", 350, 140);

doc.autoTable(columns, dataCollections.mata_kuliah, options(doc));

pengesahan(doc);
