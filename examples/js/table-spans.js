var doc = new jsPDF('p', 'pt');
doc.setFontSize(12);
doc.setTextColor(0);
doc.setFontStyle('bold');
doc.text('Col and row span', 40, 50);
var data = getData(20);
data.sort(function (a, b) {
    return parseFloat(b.expenses) - parseFloat(a.expenses);
});
doc.autoTable(getColumns(), data, {
    theme: 'grid',
    startY: 60,
    drawRow: function (row, data) {
        // Colspan
        doc.setFontStyle('bold');
        doc.setFontSize(10);
        if (row.index === 0) {
            doc.setTextColor(200, 0, 0);
            doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
            doc.autoTableText("Priority Group", data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
                halign: 'center',
                valign: 'middle'
            });
            data.cursor.y += 20;
        } else if (row.index === 5) {
            doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
            doc.autoTableText("Other Groups", data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
                halign: 'center',
                valign: 'middle'
            });
            data.cursor.y += 20;
        }
    },
    drawCell: function (cell, data) {
        // Rowspan
        if (data.column.dataKey === 'id') {
            if (data.row.index % 5 === 0) {
                doc.rect(cell.x, cell.y, data.table.width, cell.height * 5, 'S');
                doc.autoTableText(data.row.index / 5 + 1 + '', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
                    halign: 'center',
                    valign: 'middle'
                });
            }
            return false;
        }
    }
});