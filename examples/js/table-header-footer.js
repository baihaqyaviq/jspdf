var doc = new jsPDF('p', 'pt');

var header = function (data) {
    doc.setFontSize(20);
    doc.setTextColor(40);
    doc.setFontStyle('normal');
    doc.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
    doc.text("Report", data.settings.margin.left + 35, 60);
};

var totalPagesExp = "{total_pages_count_string}";
var footer = function (data) {
    var str = "Page " + data.pageCount;
    // Total page number plugin only available in jspdf v1.0+
    if (typeof doc.putTotalPages === 'function') {
        str = str + " of " + totalPagesExp;
    }
    doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 30);
};

var options = {
    beforePageContent: header,
    afterPageContent: footer,
    margin: {top: 80}
};
doc.autoTable(getColumns(), getData(40), options);

// Total page number plugin only available in jspdf v1.0+
if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
}